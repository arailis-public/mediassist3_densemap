#ifndef CLOUD_TYPES_H
#define CLOUD_TYPES_H

#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree_search.h>
#include <pcl/kdtree/kdtree_flann.h>

struct LapPointType
{
	PCL_ADD_POINT4D;                  // preferred way of adding a XYZ+padding
	PCL_ADD_RGB;
	std::uint32_t merge_cnt;
	std::uint32_t miss_cnt;
	std::uint32_t segmentation_cnt;
	float reliability;
	std::uint32_t sequence;
	double view_pose_x;
	double view_pose_y;
	double view_pose_z;
	int in_view_frustrum;
	float eraseScore;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment

struct LapNormalPointType
{
	PCL_ADD_POINT4D;                  // preferred way of adding a XYZ+padding
	PCL_ADD_RGB;
  PCL_ADD_NORMAL4D;
	std::uint32_t merge_cnt;
	std::uint32_t segmentation_cnt;
	float reliability;
	std::uint32_t sequence;
	double view_pose_x;
	double view_pose_y;
	double view_pose_z;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment


POINT_CLOUD_REGISTER_POINT_STRUCT (LapPointType,
	(float, x, x)
	(float, y, y)
	(float, z, z)
	(std::uint32_t, rgb, rgb)
	(std::uint32_t, merge_cnt, merge_cnt)
	(std::uint32_t, segmentation_cnt, segmentation_cnt)
	(float, reliability, reliability)
	(std::uint32_t, sequence, sequence)
	(double, view_pose_x, view_pose_x)
	(double, view_pose_y, view_pose_y)
	(double, view_pose_z, view_pose_z)
)
POINT_CLOUD_REGISTER_POINT_STRUCT (LapNormalPointType,
	(float, x, x)
	(float, y, y)
	(float, z, z)
  (float, normal_x, normal_x)
  (float, normal_y, normal_y)
  (float, normal_z, normal_z)
	(std::uint32_t, rgb, rgb)
	(std::uint32_t, merge_cnt, merge_cnt)
	(std::uint32_t, segmentation_cnt, segmentation_cnt)
	(float, reliability, reliability)
	(std::uint32_t, sequence, sequence)
	(double, view_pose_x, view_pose_x)
	(double, view_pose_y, view_pose_y)
	(double, view_pose_z, view_pose_z)
)


typedef pcl::PointCloud<pcl::PointXYZRGB> PCL_Cloud;
typedef pcl::PointCloud<pcl::PointXYZRGBL> PCL_CloudLabeled;
typedef pcl::PointCloud<LapPointType> Lap_Cloud;
typedef pcl::octree::OctreePointCloudSearch<LapPointType> Lap_Octree;
typedef pcl::KdTreeFLANN<LapPointType> Lap_KdTree;


#endif //CLOUD_TYPES_H
