// written by Peter K in May 2019
// adopted by Micha Pfeiffer
// Dense Map node

#ifndef LAPAROSCOPIC_DENSE_MAP_DENSE_MAP_H
#define LAPAROSCOPIC_DENSE_MAP_DENSE_MAP_H

#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/random_sample.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_srvs/Trigger.h>

#include <dynamic_reconfigure/server.h>
#include <mediassist3_densemap/DenseMapConfig.h>
#include <mediassist3_densemap/cloud_types.h>
#include <mediassist3_densemap/Map.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <mediassist3_densemap/ViewFrustum.h>
#include <mediassist3_densemap/Cache.h>


namespace mediassist3_densemap
{

using namespace geometry_msgs;
using namespace sensor_msgs;
using namespace message_filters;
using namespace nav_msgs;

typedef dynamic_reconfigure::Server<DenseMapConfig> DynConfigServerType;

class DenseMapNode
{
public:
	DenseMapNode();
	~DenseMapNode();
	void onInit(ros::NodeHandle &nh, ros::NodeHandle &private_nh);
	void newConfigCallback( DenseMapConfig &config, std::uint32_t level);

protected:
	pcl::IndicesPtr removeIndices = pcl::IndicesPtr( new std::vector<int>() );

private:
	void cleanMapLocal();
	void cleanMap();
	//void erasePointsFromRemoveIndices();
	void newPoseCallback(
			const PoseStampedConstPtr& pose_msg );
	void newPointsCallback(
			const PointCloud2ConstPtr& point_cloud_2_msg );
	void newMaskCallback(
			const ImageConstPtr& segmentation_mask );
	void imageSyncCallback(
			const ImageConstPtr& left_msg,
                        const CameraInfoConstPtr& left_info_msg,
                        const ImageConstPtr& right_msg,
                        const CameraInfoConstPtr& right_info_msg );
	void unboundedCallback(
                        const PoseStampedConstPtr& pose_msg,
                        const PointCloud2ConstPtr& point_cloud_2_msg,
			const CameraInfoConstPtr& left_info_msg);
        void unboundedCallback(
                        const PoseStampedConstPtr& pose_msg,
                        const PointCloud2ConstPtr& point_cloud_2_msg,
                        const ImageConstPtr& segmentation_mask,
		       	const CameraInfoConstPtr& left_info_msg	);
	void parseNewData(
			const PoseStampedConstPtr& pose_msg,
			const PointCloud2ConstPtr& point_cloud_2_msg,
			const ImageConstPtr& segmentationMask = ImageConstPtr());
	void parseCaches();
	void resetCaches();
	void newTrajectory(
			const PathPtr& path );

	bool resetService( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res );
	void resetAll();

	unsigned int extractMapID( std::string frameID );

	void publishCurrentMap();
	void publishDebugTrajectories( ros::Time timestamp );
	void publishDebugViewFrustrum( const ViewFrustum& viewFrustum );

	Subscriber<Image>* sub_img_left_ = NULL;
	Subscriber<CameraInfo>* sub_img_left_info_ = NULL;
	Subscriber<Image>* sub_img_right_ = NULL;
	Subscriber<CameraInfo>* sub_img_right_info_ = NULL;
	Subscriber<PoseStamped>* sub_camera_pose_ = NULL;
	Subscriber<PointCloud2>* sub_point_cloud_ = NULL;
	Subscriber<Image>* sub_mask_image_ = NULL;
	Subscriber<Path>* sub_trajectories_ = NULL;

	ros::ServiceServer serviceSLAMReset;

	ros::Publisher pub_;
	ros::Publisher pub_req_disp_left_;
	ros::Publisher pub_req_disp_left_info_;
	ros::Publisher pub_req_disp_right_;
	ros::Publisher pub_req_disp_right_info_;

	ros::Publisher pub_debug_frustum_;
	ros::Publisher pub_debug_trajectories_;

	TimeSynchronizer<Image,CameraInfo,Image,CameraInfo,PoseStamped>* time_sync_pose_ = NULL;

	TimeSynchronizer<Image,CameraInfo,Image,CameraInfo>* image_data_sync_ = NULL;
	TimeSynchronizer<PoseStamped,PointCloud2,CameraInfo>* unbounded_sync_ = NULL;
        TimeSynchronizer<PoseStamped,PointCloud2,Image,CameraInfo>* unbounded_sync_seg_ = NULL;

	std::map<unsigned int, Map*> maps;

	PoseStampedConstPtr cachedPose;
	PointCloud2ConstPtr cachedPoints;
	ImageConstPtr cachedMask;

	ros::Time prevRequestTime;

	int mostRecentMapID;

	int cutoff_left_;
	int cutoff_right_;
	int cutoff_top_;
	int cutoff_bottom_;
	bool input_in_mm_;

	bool record;

	float inputPointsAmount;
	float minMergeRatio;
	int minMergeCount;
	bool publishOnlySegmented;
	float minSegmentationReliability;
	bool useSegmentationMask;
	bool unboundedSync;
	bool smoothOutputCloud;
	float smoothingRadius;
	float maxPixelDepth;
	float depthRemovalThresh;

  int minPixelBrightness;
  int maxPixelBrightness;
  bool publishViewFrustum;
  bool highlightViewFrustum;
  bool highlightSegmentation;

  Eigen::Affine3d leftCamIntrinsics;

	// Dynamic reconfiguration:
	std::shared_ptr<DynConfigServerType> dynConfigServer;

  ros::Time prevMapPublishTime;

	Cache inputCache;
};

};
#endif
