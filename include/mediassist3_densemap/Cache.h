#ifndef CACHE_H
#define CACHE_H

#include <set>
#include <ros/ros.h>
#include <tf_conversions/tf_eigen.h>
#include <mediassist3_densemap/cloud_types.h>
#include <nav_msgs/Path.h>
#include <visualization_msgs/Marker.h>
#include <mediassist3_densemap/ViewFrustum.h>

namespace mediassist3_densemap
{

class CacheEntry {
	public:
		CacheEntry( ros::Time timestamp,
				ViewFrustum viewFrustum,
				PCL_CloudLabeled::Ptr cloud,
				unsigned int ID,
				float maxDepth );

		bool operator<(const CacheEntry &other) const { return timestamp < other.timestamp; }

		ros::Time timestamp;
		PCL_CloudLabeled::Ptr cloud;
		ViewFrustum viewFrustum;
		//const sensor_msgs::ImageConstPtr& segmentation_msg;
		unsigned int ID;
		float maxDepth;
};


class Cache
{
	public:
		Cache() {};
		~Cache() {};

		void addEntry( CacheEntry cacheEntry );
		void updateTrajectory( nav_msgs::PathPtr path, unsigned int ID );
		void clear();
	
		// Use a set, as that is sorted. Since CacheEntry has a < operator implemented, the
		// entries are sorted by that comparison (i.e. the timestamp).	
		std::set<CacheEntry> cache;

		void debugPrint();

		visualization_msgs::Marker getDebugTrajectory( ros::Time timestamp,
				std_msgs::ColorRGBA col, unsigned int ID );
};

}

#endif
