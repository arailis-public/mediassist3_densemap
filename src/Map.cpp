
#include <mediassist3_densemap/Map.h>
#include <mediassist3_densemap/cloud_types.h>
#include <pcl/surface/mls.h>
#include <random>

#include <boost/version.hpp>
#if ((BOOST_VERSION / 100) % 1000) >= 53
#include <boost/thread/lock_guard.hpp>
#endif

#include <algorithm>
#include <iostream>
#include <chrono>

#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/octree/octree_search.h>
#include <pcl/conversions.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>
#include <tf_conversions/tf_eigen.h>


#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>

#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>

#include "cv_bridge/cv_bridge.h"
#include <opencv2/opencv.hpp>

#include <dynamic_reconfigure/server.h>
#include <mediassist3_densemap/DenseMapConfig.h>

#include <cmath>
#include <fstream>

using namespace std::chrono;

namespace mediassist3_densemap
{


Map::Map( unsigned int ID, float inputPointsAmount, int minBrightness, int maxBrightness, float maxDepth, float depthRemovalThreshold )
{
	mID = ID;
	cutoffLeft = cutoffTop = cutoffRight = cutoffBottom = 0;
	this->inputPointsAmount = inputPointsAmount;
  minPixelBrightness = minBrightness;
  maxPixelBrightness = maxBrightness;
	maxPixelDepth = maxDepth;
	depthRemovalThresh = depthRemovalThreshold;
	reset();
}

Map::~Map()
{

}

void Map::reset()
{
	ROS_INFO_STREAM( "Resetting map " << mID );
	pclCloudPtr = boost::make_shared<Lap_Cloud>();
	buildTree();
}

void Map::buildTree()
{
  // Only build the tree if we have any points in the map!
  if( pclCloudPtr->size() > 0 )
  {
		ROS_INFO_STREAM( "Building new tree for map " << mID );
	  pclTreePtr = boost::make_shared<Lap_KdTree>();
	//pclTreePtr->defineBoundingBox(-1.0,-1.0,-1.0,1.0,1.0,1.0);
    pclTreePtr->setInputCloud( pclCloudPtr );
	//pclTreePtr->addPointsFromInputCloud();
  }
}

CacheEntry Map::addFrame( ros::Time timestamp, tf::Pose pose_tf, Eigen::Affine3d camIntrinsics,
  PCL_Cloud::Ptr input,
	const sensor_msgs::ImageConstPtr& segmentation_msg )
{
	ROS_INFO_STREAM( "Number of points in input: " << input->size() );

	maxPixelCol = input->width - cutoffRight - 1;
	maxPixelRow = input->height - cutoffBottom - 1;
	
	ROS_INFO_STREAM( "Cutoff: " << cutoffLeft << " " << cutoffRight << "  " << cutoffTop << " " << cutoffBottom );

  auto start = high_resolution_clock::now();
	auto stop = high_resolution_clock::now();
  auto duration = duration_cast<milliseconds>(stop - start);

	// Extract the corner points of the view frustrum:
	// top left:
	pcl::PointXYZRGB p1 = input->at( cutoffLeft, cutoffTop );
	// top right:
	pcl::PointXYZRGB p2 = input->at( maxPixelCol, cutoffTop );
	// bottom right:
	pcl::PointXYZRGB p3 = input->at( maxPixelCol, maxPixelRow );
	// bottom left:
	pcl::PointXYZRGB p4 = input->at( cutoffLeft, maxPixelRow );

	// Set the cam pose in map space
	viewFrustum.setCameraPose( pose_tf );
	// Set the corner points of the view, which are currently still in view space.
	// The viewFrustum will convert them to map space (using the pose_tf above) once needed.
	// Since the camPose may change when the trajectory is corrected/updated, we store the 
	// corner points in view space (where they don't change)
	viewFrustum.setCornerPoints( p1, p2, p3, p4 );

	viewFrustum.setCamIntrinsics( camIntrinsics );

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration1: " << duration.count() );
  start = stop;

	// Compute map space coordinates:	
	viewFrustum.updateMapSpace();

	// Remember original frame:
	viewFrustum.setOriginalImage( input );

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration2: " << duration.count() );
  start = stop;


	//auto t3 = Clock::now();
	int size_input = input->size();

	if(rand_indices.size() != size_input) {
		rand_indices.clear();
		for (int i=0; i < size_input; ++i)
			rand_indices.push_back(i);
	}
	std::random_shuffle ( rand_indices.begin(), rand_indices.end() );

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration3: " << duration.count() );
  start = stop;

	PCL_CloudLabeled::Ptr random_subset_cloud(new PCL_CloudLabeled());

	ROS_INFO_STREAM("Random indices: " << rand_indices.size() );
	ROS_INFO_STREAM("Input indices: " << input->size() );

	float maxDepth = 0;
	//float maxDepthGradient = 1e-3;

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration4: " << duration.count() );
  start = stop;

	//auto t4 = Clock::now();
  int numPointsFromInput = int(rand_indices.size()*inputPointsAmount);
  ROS_INFO_STREAM("Will use " << numPointsFromInput << " points from input (" << inputPointsAmount*100 
      << "%)" );
	for(int j = 0; j < int(rand_indices.size()*0.1); j++) {
		unsigned int i = rand_indices[j];
		//ROS_DEBUG_STREAM(i);
		pcl::PointXYZRGB* point = &input->at(i);
		if(std::isnan(point->x) || std::isnan(point->y) || std::isnan(point->z) )
		{
			//ROS_DEBUG_STREAM("\tnan");
			continue;
		}
		int row = (int) (i / input->width);
		int col = i - row*input->width;

		if( row % 2 != 0 || col % 2 != 0 )
		{
			continue;
		}

		// Check if this is a point which shouldn't be ignored (due to being outside
		// the valid area defined by the cutoff values):
		if( row < cutoffTop || row > maxPixelRow || col < cutoffLeft || col > maxPixelCol )
		{
			//ROS_DEBUG_STREAM("\tcut " << row <<  " " << col);
			continue;
		}
	
		// TODO: Merge with cutoffTop etc condition:
		//if( row < 1 || row > input->height-2 || col < 1 || col > input->width-2 )
		//{
			//ROS_DEBUG_STREAM("\tcut " << row <<  " " << col);
			//continue;
		//}
		/*std::cout << "diff" << std::endl;
		std::cout << "\t" <<	abs( point->z - input->at( col+1, row ).z ) << std::endl;
		std::cout << "\t" <<	abs( point->z - input->at( col-1, row ).z ) << std::endl;
		std::cout << "\t" <<	abs( point->z - input->at( col, row+1 ).z ) << std::endl;
		std::cout << "\t" <<	abs( point->z - input->at( col, row-1 ).z ) << std::endl;*/
		/*if( abs( point->z - input->at( col+1, row ).z ) > maxDepthGradient )
			continue;
		if( abs( point->z - input->at( col-1, row ).z ) > maxDepthGradient )
			continue;
		if( abs( point->z - input->at( col, row+1 ).z ) > maxDepthGradient )
			continue;
		if( abs( point->z - input->at( col, row-1 ).z ) > maxDepthGradient )
			continue;*/

		// Only consider points which are "close" to the camera:
		if( point->z > maxPixelDepth )
			continue;

		// Don't add as new point if it was too dark!
		int sum = point->r + point->g + point->b;
		if( sum < minPixelBrightness*3 )
			continue;
		if( sum > maxPixelBrightness*3 )
			continue;

  
		pcl::PointXYZRGBL pointL;
		pointL.x = point->x;
		pointL.y = point->y;
		pointL.z = point->z;
		pointL.r = point->r;
		pointL.g = point->g;
		pointL.b = point->b;
		if(segmentation_msg)
			pointL.label = (short)(segmentation_msg->data[ row* segmentation_msg->step + col ] > 0);
		else
			pointL.label = 0;
		random_subset_cloud->push_back(pointL);
		if( pointL.z > maxDepth )
			maxDepth = pointL.z;
	}
	ROS_INFO_STREAM( "Number of points taken from new frame: " << random_subset_cloud->size() );
	ROS_INFO_STREAM( "Maximum depth in frame: " << maxDepth );

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration5: " << duration.count() );
  start = stop;


	/*now = std::chrono::system_clock::now();
	nowMS = std::chrono::time_point_cast<std::chrono::milliseconds>(now);
	long nowMSLong = nowMS.time_since_epoch().count();		// time in MS as integer value*/

	// Cache the frame:
	CacheEntry cacheEntry( timestamp, viewFrustum, random_subset_cloud, mID, maxDepth );

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6: " << duration.count() );
  start = stop;

	mergeFrame( viewFrustum, random_subset_cloud, maxDepth );

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration7: " << duration.count() );
  start = stop;

	return cacheEntry;
}

void Map::mergeFrame( const ViewFrustum& viewFrustum, PCL_CloudLabeled::Ptr input, float maxDepth )
{
  ROS_DEBUG_STREAM( "Start merging frame into map" );
	tf::Pose pose_tf = viewFrustum.getCameraPose();

	auto start = high_resolution_clock::now();
	auto stop = high_resolution_clock::now();
  auto duration = duration_cast<milliseconds>(stop - start);



	// rotate pointcloud
	tf::Quaternion qz(tf::Vector3(0,0,1),-M_PI*0.5f);
	tf::Quaternion qy(tf::Vector3(0,1,0),M_PI*0.5f);
	pose_tf.setRotation( pose_tf.getRotation()*qy*qz );
	PCL_CloudLabeled::Ptr transCloud(new PCL_CloudLabeled());

	Eigen::Affine3d transform_view_to_map;
	tf::poseTFToEigen ( pose_tf, transform_view_to_map);
	pcl::transformPointCloud(*input, *transCloud, transform_view_to_map);

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.1: " << duration.count() );
  start = stop;

	//std::vector<LapPointType> newPoints;
	Lap_Cloud::Ptr newPoints = boost::make_shared<Lap_Cloud>();

	// Find points which intersect the view frustum, (to potentially remove them later):
	/*std::vector<std::size_t> intersectionIndices;
	viewFrustum.intersect( pclCloudPtr, &intersectionIndices, maxDepth );
	ROS_INFO_STREAM( "Points in view frustrum: " << intersectionIndices.size() );*/


  // Remember how many points were in the cloud before adding new ones:
  long numPrevMapPoints = pclCloudPtr->size();

	float mergeDistSquared = 0.005*0.005;

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.2: " << duration.count() );
  start = stop;

	//auto t5 = Clock::now();
	for( unsigned int i = 0; i < transCloud->size(); i++ )
	{
		pcl::PointXYZRGBL* p = &transCloud->at(i);

		LapPointType point;
		point.x = p->x;
		point.y = p->y;
		point.z = p->z;
		if( point.x > -1 && point.x < 1 &&
				point.y > -1 && point.y < 1 &&
				point.z > -1 && point.z < 1)
		{

			// Don't add as new point if it was too dark!
			/*int sum = p->r + p->g + p->b;
			if( sum < minPixelBrightness*3 )
				continue;
			if( sum > maxPixelBrightness*3 )
				continue;*/

			//std::vector<int> foundPointIndices;
			pcl::Indices inds;
			std::vector<float> squaredDists;
			squaredDists.resize(1);
			inds.resize(1);
			bool found = false;
			/*if( pclTreePtr->getLeafCount() > 0 )
			{*/
      if( pclTreePtr && pclTreePtr->getInputCloud()->size() > 0 )
      {
				found = pclTreePtr->nearestKSearch( point, 1, inds, squaredDists );
				// If closest point is further than this threshold, don't consider it a match:
				if( found && squaredDists[0] > mergeDistSquared )
					found = false;
      }
			//}
			
			//if( !pcl_octree_ptr_->isVoxelOccupiedAtPoint( point ) )
			point.r = p->r;
			point.g = p->g;
			point.b = p->b;
			point.merge_cnt = 1;
			point.reliability = .0f;
			/*point.view_pose_x = pose_tf.getOrigin().x();
			point.view_pose_y = pose_tf.getOrigin().y();
			point.view_pose_z = pose_tf.getOrigin().z();*/
			// Set the segmentation count to the label. If we decide to weigh
			// the first image more (by setting merge_cnt > 1), also consider this
			// in the segmentation count:
			point.segmentation_cnt = p->label*point.merge_cnt;
			point.miss_cnt = 0;
			point.reliability = (float)point.segmentation_cnt/((float)point.merge_cnt);
			//point.lastUpdateTime = nowMSLong;

			if( found )
			{
				LapPointType* ePoint = &pclCloudPtr->at(inds[0]);

				point.merge_cnt = ePoint->merge_cnt + 1;

				// Compare the brighnesses. Choose the color of the point which is closer to a
				// mid-tone-grey (128+128+128 = 384)
				int brightnessOld = ePoint->r + ePoint->g + ePoint->b;
				int brightnessNew = p->r + p->g + p->b;
				if( std::abs(brightnessNew - 384) < std::abs(brightnessOld - 384) )
				{
					point.r = (ePoint->r*0.25 + p->r*0.75);
					point.g = (ePoint->g*0.25 + p->g*0.75);
					point.b = (ePoint->b*0.25 + p->b*0.75);
				} else {
					point.r = (ePoint->r*0.75 + p->r*0.25);
					point.g = (ePoint->g*0.75 + p->g*0.25);
					point.b = (ePoint->b*0.75 + p->b*0.25);
				}

				point.segmentation_cnt = ePoint->segmentation_cnt + p->label;
				point.reliability = (float)(point.segmentation_cnt)/((float)point.merge_cnt);
				//std::cout << "merge " << ePoint->segmentation_cnt << " " << p->label << std::endl;
				//std::cout << "" << point.segmentation_cnt << " " << point.merge_cnt << std::endl;
				// TODO: count misses and modify hue after some time?
			}
			//pclTreePtr->addPointToCloud( point, pclCloudPtr );
			newPoints->push_back( point );
		}
	}

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.3: " << duration.count() );
  start = stop;


	//auto t6 = Clock::now();
	ROS_INFO_STREAM( "Number of points in map: " << pclCloudPtr->size() );

	Eigen::Affine3d transform_map_to_view = transform_view_to_map.inverse();
	Eigen::Affine3d transform_view_to_image = viewFrustum.getCamIntrinsics();
	Eigen::Affine3d transform_view_projection = transform_view_to_image*transform_map_to_view;
	//pcl::transformPointCloud(*input, *transCloud, transform_view_projection);

  /*std::cout << "map to view: " << transform_map_to_view.matrix() << std::endl;
  std::cout << "view to image: " << transform_view_to_image.matrix() << std::endl;
  std::cout << "projection: " << transform_view_projection.matrix() << std::endl;*/

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.4: " << duration.count() );
  start = stop;

  pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
  pcl::ExtractIndices<LapPointType> extract;
	PCL_Cloud::Ptr orig = viewFrustum.getOriginalImage();
  for (long index = 0; index < numPrevMapPoints; index++)
  {
		//int index = intersectionIndices[i];
		LapPointType* p = &pclCloudPtr->at(index);
    Eigen::Vector4d pt( p->x, p->y, p->z, 1 );
    Eigen::Vector4d pt_in_imagespace = transform_view_projection * pt;
    //std::cout << "depth: " << pt_in_imagespace(2) << std::endl;
		//std::cout << "point: " << pt_in_imagespace << std::endl;

		float x = pt_in_imagespace(0)/pt_in_imagespace(2);
		int col = int( x + 0.5 );
		float y = pt_in_imagespace(1)/pt_in_imagespace(2);
		int row = int( y + 0.5 );
    
    /*std::cout << row << " " << col <<  " " << std::endl;
    std::cout << cutoffTop << " " << cutoffLeft <<  " " << maxPixelRow << " " << maxPixelCol << std::endl;
		if( row < cutoffTop || row > maxPixelRow || col < cutoffLeft || col > maxPixelCol )
    {
      std::cout << "\tOutlier" << std::endl;
      p->r = 0;
      p->g = 0;
      p->b = 255;
    } else {
      std::cout << "\tInlier" << std::endl;
      p->r = 255;
      p->g = 255;
      p->b = 0;
    }

    if( true )    // DEBUG
      continue;*/

		if( row < cutoffTop || row > maxPixelRow || col < cutoffLeft || col > maxPixelCol )
      continue;


		//std::cout << "Pixel: " << pixelX << " " << pixelY << std::endl;
		//if( x >= 0 && x < orig->width && y >= 0 && y < orig->height )

    // Find the point in the depth map that is at the same x/y position in the image
    // as the current map point would be:
    pcl::PointXYZRGB* p_orig = &orig->at( col, row );

		// If the reprojected map point falls into a point which has a depth larger than the maxPixelDepth,
		// i.e. on a point that is not being used from this new image, then do no remove the map point.
		if( p_orig->z > maxPixelDepth )
			continue;

		int sum = p_orig->r + p_orig->g + p_orig->b;
		if( sum < minPixelBrightness*3 )
			continue;
		if( sum > maxPixelBrightness*3 )
			continue;

    Eigen::Vector4d pt_orig( p_orig->x, p_orig->y, p_orig->z, 1 );
    Eigen::Vector4d pt_orig_in_imagespace = transform_view_to_image * pt_orig;
    // Compare the depths:
    // If the depth of the map pixel is much larger than the pixel in the new "original" image,
    // keep it. Otherwise, it's in the space between the camera and the newly added point cloud
    // -> in this case, remove it.
    if( pt_orig_in_imagespace(2) > pt_in_imagespace(2) - depthRemovalThresh )
		{
        inliers->indices.push_back(index);		// inliers will be removed!
		}
  }

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.5: " << duration.count() );
  start = stop;


  extract.setInputCloud(pclCloudPtr);
  extract.setIndices(inliers);
  extract.setNegative(true);
  extract.filter(*pclCloudPtr);
	ROS_INFO_STREAM( "Points in map after removal: " << pclCloudPtr->size() );

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.6: " << duration.count() );
  start = stop;

	//for( auto p = newPoints.begin(); p != newPoints.end(); p++ )
   //   pclCloudPtr->push_back( p );
			//PointToCloud( *p, pclCloudPtr );
  *pclCloudPtr += *newPoints;

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.7: " << duration.count() );
  start = stop;

  buildTree();

	stop = high_resolution_clock::now();
  duration = duration_cast<milliseconds>(stop - start);
  ROS_DEBUG_STREAM( "Duration6.8: " << duration.count() );
  start = stop;


	/*if( pclCloudPtr->size() > 50000 )
	  {
	  ROS_INFO_STREAM( "\tToo many points. Cleaning map");
	  cleanMapLocal();
	  ROS_INFO_STREAM( "\tNumber of points in map: " << pclCloudPtr->size() );
	  }*/


	/*std::vector<std::size_t> intersectionIndices;
	viewFrustum.intersect( pclCloudPtr, &intersectionIndices );

	ROS_INFO_STREAM( "Points in view frustrum: " << intersectionIndices.size() );
	for( std::size_t i = 0; i < intersectionIndices.size(); i++ )
	{
		std::size_t index = intersectionIndices[i];
		pclCloudPtr->at(index).miss_cnt += 1;
	}*/

	/*std::size_t numPoints = pclCloudPtr->size();
	for ( Lap_Cloud::iterator it = pclCloudPtr->begin(); it != pclCloudPtr->end(); it++)
	{
		if( it->miss_cnt > it->merge_cnt*4 )
		{
			pclCloudPtr->erase(it--);
		}
	}
	if( numPoints != pclCloudPtr->size() )
		buildOctree();
		*/
  ROS_DEBUG_STREAM( "Done merging frame into map" );
}

void Map::rebuildAfterCacheUpdate( Cache& cache )
{
	ROS_INFO_STREAM( "Rebuilding map " << mID );

	// Start by clearing the map
	reset();
	ROS_INFO_STREAM( "Reset done " << mID );

	for( auto cacheIt = cache.cache.begin(); cacheIt != cache.cache.end(); cacheIt ++ )
	{
		if( cacheIt->ID == mID )
		{
			mergeFrame( cacheIt->viewFrustum, cacheIt->cloud, cacheIt->maxDepth );
		}
	}

	ROS_INFO_STREAM( "Done rebuilding map " << mID );
}

sensor_msgs::PointCloud2Ptr Map::getFullMap( float minMergeRatio, int minMergeCount,
		bool publishOnlySegmented, float minSegmentationReliability,
		float smoothingRadius, bool highlightViewFrustum, bool highlightSegmentation )
{
	Lap_Cloud::Ptr out_cloud(new Lap_Cloud());
	for( unsigned int i = 0; i < pclCloudPtr->size(); i++ )
	{
		float mergeRatio = (float)pclCloudPtr->at(i).merge_cnt/(float)std::max( 1, (int)pclCloudPtr->at(i).miss_cnt);
		if( mergeRatio < minMergeRatio )
			continue;
		if( pclCloudPtr->at(i).merge_cnt < minMergeCount )
			continue;


		//if( pclCloudPtr->at(i).merge_cnt < pclCloudPtr->at(i).miss_cnt )
			//continue;
		if( !publishOnlySegmented || pclCloudPtr->at(i).reliability > minSegmentationReliability )
		{
			LapPointType p = pclCloudPtr->at(i);
			if( highlightSegmentation )
			{
				p.r = 255*p.reliability;
				p.g = 255*p.reliability;
				p.b = 255*p.reliability;
			}
			if( highlightViewFrustum && p.in_view_frustrum == 1 )
			{
				p.r *= 0.5;
				p.g = (unsigned char)std::min( ((int)p.g)*1.5, 255.0 );
				p.b *= 0.5;
			}
			out_cloud->push_back( p );
		}
	}
	//auto t7 = Clock::now();
	ROS_INFO_STREAM( "Number of points to publish: " << out_cloud->size() );

	sensor_msgs::PointCloud2Ptr cloud_msg = boost::make_shared<sensor_msgs::PointCloud2>();

	if( smoothingRadius <= 0 ) {
		pcl::toROSMsg(*out_cloud,*cloud_msg);
	} else {

		ROS_INFO_STREAM( "Smoothing map before publishing. Smoothing radius: " << smoothingRadius );
		// Output has the PointNormal type in order to store the normals calculated by MLS
		pcl::PointCloud<LapNormalPointType>::Ptr out_cloud_smoothed (new pcl::PointCloud<LapNormalPointType>() );

		// Create a KD-Tree
		//pcl::KdTreeFLANN<LapPointType>::Ptr tree (new pcl::KdTreeFLANN<LapPointType>);
		pcl::search::KdTree<LapPointType>::Ptr tree (new pcl::search::KdTree<LapPointType>);

		// Init object (second point type is for the normals, even if unused)
		pcl::MovingLeastSquares<LapPointType, LapNormalPointType> mls;

		mls.setComputeNormals (true);

		// Set parameters
		mls.setInputCloud (out_cloud);
		mls.setPolynomialOrder(2);
		mls.setSearchMethod (tree);
		mls.setSearchRadius (smoothingRadius);

		// Reconstruct
		mls.process (*out_cloud_smoothed);
		//out_cloud = mls_points;
		
		pcl::toROSMsg(*out_cloud_smoothed,*cloud_msg);
	}
	return cloud_msg;

}

void Map::setCutoff( int left, int top, int right, int bottom )
{
	this->cutoffLeft = left;
	this->cutoffTop = top;
	this->cutoffRight = right;
	this->cutoffBottom = bottom;
}

/*void Map::updateTrajectory( nav_msgs::PathPtr path, unsigned int ID )
{
	// Remove all previously set points:
	reset();
	ROS_ERROR_STREAM( "Updating trajectory for map: " << ID );

		// Re-apply everything in the cache and publish:
	for( cacheIt = inputCache.begin(); cacheIt != inputCache.end(); cacheIt ++ )
	{
		ROS_INFO_STREAM("Re-adding cached frame:");
		mergeFrame(
				cacheIt->pose_tf,
				cacheIt->input );
	}
}*/

/*visualization_msgs::Marker Map::getDebugTrajectory( ros::Time timestamp, std_msgs::ColorRGBA col )
{
	visualization_msgs::Marker marker;
	marker.header.stamp = timestamp;
	marker.header.frame_id = "map";
	marker.ns = "trajectory_densemap";
	marker.id = ID;
	marker.action = 0;
	marker.type = visualization_msgs::Marker::LINE_STRIP;
	marker.pose.orientation.w = 1;
	marker.frame_locked = true;
	marker.lifetime = ros::Duration(0);
	marker.color = col;
	marker.scale.x = 0.0005;

	ROS_WARN_STREAM("Republishing map " << ID );

	for( auto cacheIt = inputCache.begin(); cacheIt != inputCache.end(); cacheIt++ )
	{
		tf::Pose pose_tf = cacheIt->pose_tf;
		auto pos = cacheIt->pose_tf.getOrigin();
		geometry_msgs::Point p;
		p.x = pose_tf.getOrigin()[2];
		p.y = -pose_tf.getOrigin()[0];
		p.z = -pose_tf.getOrigin()[1];
		marker.points.push_back( p );
		ROS_WARN_STREAM( "Republishing: " <<
				pose_tf.getOrigin()[0] << " " <<
				pose_tf.getOrigin()[1] << " " <<
				pose_tf.getOrigin()[2] );
	}
	return marker;
}*/

/* Returns true if the path and the cached poses share at least one common timestamp.
 * Assumes that both the cache and the path have sorted time stamps! */
/*bool Map::checkTrajectoryOverlap( nav_msgs::PathPtr path )
{
	auto cacheIt = inputCache.begin();
	auto pathIt = path->poses.begin();

	while( cacheIt != inputCache.end() && pathIt != path->poses.end() )
	{
		ros::Time cacheTimestamp = cacheIt->timestamp;
		ros::Time pathTimestamp = pathIt->header.stamp;
		// ORB SLAM3 needs the timestamps to be in float/double format. That's why
		// "toSec" was previously called on the timestamps. Sadly, when converted
		// back, the timestamps are no longer precise enough to be matched (the nsec
		// part is changed slightly). Thus, we compare toSec values instead, which
		// seems to work.
		if( cacheTimestamp.toSec() == pathTimestamp.toSec() ) {
			return true;
		} else if( cacheTimestamp > pathTimestamp ) {
			pathIt ++;
		} else {
			cacheIt ++;
		}

	}
	return false;

}*/

}
