#include <mediassist3_densemap/Cache.h>

using namespace mediassist3_densemap;

CacheEntry::CacheEntry( ros::Time timestamp,
		ViewFrustum viewFrustum,
		PCL_CloudLabeled::Ptr cloud,
		unsigned int ID,
		float maxDepth )
{
	this->timestamp = timestamp;
	this->viewFrustum = viewFrustum;
	this->cloud = cloud;
	this->ID = ID;
	this->maxDepth = maxDepth;
}

void Cache::addEntry( CacheEntry cacheEntry )
{
	cache.insert( cacheEntry );
	while( cache.size() > 20 )
		cache.erase( cache.begin() );

	std::cout << "CACHE Size: " << cache.size() << std::endl;
}

void Cache::updateTrajectory( nav_msgs::PathPtr path, unsigned int ID )
{
	// update the poses of each frame:
	std::set<CacheEntry>::iterator cacheIt = cache.begin();
	auto pathIt = path->poses.begin();

	int matched = 0;

	while( cacheIt != cache.end() && pathIt != path->poses.end() )
	{
		ros::Time cacheTimestamp = cacheIt->timestamp;
		ros::Time pathTimestamp = pathIt->header.stamp;
		// ORB SLAM3 needs the timestamps to be in float/double format. That's why
		// "toSec" was previously called on the timestamps. Sadly, when converted
		// back, the timestamps are no longer precise enough to be matched (the nsec
		// part is changed slightly). Thus, we compare toSec values instead, which
		// seems to work.
		if( cacheTimestamp.toSec() == pathTimestamp.toSec() ) {

			// Convert the message pose to a TF pose:
			tf::Pose newPose;
			tf::poseMsgToTF(pathIt->pose, newPose);

			// Update the camera pose:
			ViewFrustum frustum = cacheIt->viewFrustum;
			frustum.setCameraPose( newPose );
			frustum.updateMapSpace();

			// std::set entries are immutable, so construct a new one and replace the old one:
			CacheEntry newEntry( cacheTimestamp, frustum, cacheIt->cloud, ID, cacheIt->maxDepth );
			cache.erase( cacheIt );	// remove old
			auto pair = cache.insert( newEntry );		// insert new and get iterator to it
			cacheIt = pair.first;

			cacheIt ++;
			pathIt ++;
			matched ++;
		} else if( cacheTimestamp > pathTimestamp ) {
			pathIt ++;
		} else {
			cacheIt ++;
			//ROS_WARN_STREAM("Skipped pose during trajectory update - it no longer exists in the SLAM map?");
			// TODO: Remove this entry?
		}
	}
	ROS_INFO_STREAM("\tMatched " << matched << " of " << cache.size() << " timestamps in trajectory of map " << ID );
}

void Cache::clear()
{
	cache.clear();	
}

visualization_msgs::Marker Cache::getDebugTrajectory( ros::Time timestamp,
		std_msgs::ColorRGBA col, unsigned int ID )
{
	visualization_msgs::Marker marker;
	marker.header.stamp = timestamp;
	marker.header.frame_id = "map";
	marker.ns = "trajectory_densemap";
	marker.id = ID;
	marker.action = visualization_msgs::Marker::ADD;
	marker.type = visualization_msgs::Marker::LINE_STRIP;
	marker.pose.orientation.w = 1;
	marker.frame_locked = true;
	marker.lifetime = ros::Duration(0);
	marker.color = col;
	marker.scale.x = 0.0005;

	//ROS_WARN_STREAM("Republishing map " << ID );

	for( auto cacheIt = cache.begin(); cacheIt != cache.end(); cacheIt++ )
	{
		if( cacheIt->ID == ID )
		{
			tf::Pose pose_tf = cacheIt->viewFrustum.getCameraPose();
			geometry_msgs::Point p;
			/*p.x = pose_tf.getOrigin()[2];
			p.y = -pose_tf.getOrigin()[0];
			p.z = -pose_tf.getOrigin()[1];*/
			p.x = pose_tf.getOrigin()[0];
			p.y = pose_tf.getOrigin()[1];
			p.z = pose_tf.getOrigin()[2];
			marker.points.push_back( p );
			/*ROS_WARN_STREAM( "Republishing: " <<
			  pose_tf.getOrigin()[0] << " " <<
				pose_tf.getOrigin()[1] << " " <<
				pose_tf.getOrigin()[2] );*/
		}
	}

	// Must contain at least two points to be a valid marker:
	if( marker.points.size() < 2 )
		marker.action = visualization_msgs::Marker::DELETE;

	return marker;
}


