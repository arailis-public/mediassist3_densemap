DenseMap
=========

The DenseMap module is a tool for creating 3D point cloud maps of environments. Ultimately, it does this by integrating multiple point clouds, observed from different camera poses. The method considers the positions and hue of each point in the map, and records repeated observances of the same point. When the tally of observances exceeds a threshold, the point is included in the final map. If, at a later time, points which should be seen in the view of the camera are unseen, they are removed from the final map as they are likely to be the result of noise.

## Installation
Clone this repository into the `/src` folder of your catkin workspace.
The structure should be similar to this:

├── catkin_ws/

│——├── src/

│——│——├── *other projects*

│——│——├── mediassist3_densemap

└——└——└── CMakeLists.txt


## Compile
Run `catkin_make` inside the root of your catkin workspace folder.

## Run

### General Usage
In order to use the densemap, at least three pieces of information must be made available to the system:
- Camera Pose of PoseStamped type (with frame_id 'map')
- Point Cloud of PointCloud2 type (with rgbxyz data)
- Camera Info relating to the left camera
These may all be set within 'dense_map.launch' in the launch folder. This listens to the information provided and assembles point cloud representation using it.

### Testing on rosbags
As an example, you may test the module using a bag which we provide containing sets of clouds with associated poses and camera info. This bag (large) can be downloaded from: https://caruscloud.uniklinikum-dresden.de/index.php/s/9pypSHbNcsSi5wz 

To run the demo:
- `roslaunch mediassist3_densemap dense_map_example.launch`
- `rosbag play <path/to/bag>`

This should give a lot of output on the screen and if you're lucky, `rostopic hz /dense_map/densemap` creates some nice output.

## Visualization

The resulting densemap can then be visualised using rviz. 
With the Example bag, this should look as follows: 

![DensemapExample](DensemapExample.mp4 "DensemapExample")

